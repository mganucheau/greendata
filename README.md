Using Docker
============

Build, if needed

    docker-compose build

Start everything:

    docker-compose up -d

If you need to open a shell on one of the containers:

    docker exec -it <container id> bash

eg.

    docker exec -it greendata_web_1 bash

Deploy
======

    ssh -A dhc-user@clickcleanapi.org # warning! agent forwarding is a security risk!
    cd /app/greendata
    git pull
    docker-compose build web
    docker-compose up --no-deps -d web

Server Configuration
====================

    sudo aptitude update
    sudo aptitude -y upgrade
    # Add docker repository key to apt-key for package verification
    sudo sh -c "wget -qO- https://get.docker.io/gpg | apt-key add -"
    # Add the docker repository to aptitude sources:
    sudo sh -c "echo deb http://get.docker.io/ubuntu docker main\
    > /etc/apt/sources.list.d/docker.list"
    # Update the repository with the new addition:
    sudo aptitude update
    # Finally, download and install docker:
    sudo aptitude install lxc-docker

Install docker-compose:

    sudo -i
    curl -L https://github.com/docker/compose/releases/download/1.2.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose
    exit

Enable UFW port forwarding: replace `DEFAULT_FORWARD_POLICY="DROP"` with
`"ACCEPT"` in `/etc/default/ufw`, and then:

    sudo ufw reload

Create a place to put the app:

    sudo mkdir /app

Give it useful permissions:

    sudo groupadd app
    sudo usermod -a -G app `whoami`
    sudo chmod 770 /app
    sudo chgrp app /app

You'll need to log out and log back in to update your groups.

Add your SSH key to the git repo, and:

    cd /app
    git clone git clone git@bitbucket.org:mganucheau/greendata.git

Then build the containers:

    cd greendata
    docker-compose build

If you get an error like:

    Couldn't connect to Docker daemon at http+unix://var/run/docker.sock - is it running?

Change `DOCKER_OPTS` in `/etc/default/docker` to

    DOCKER_OPTS="-H tcp://127.0.0.1:4243 -H unix:///var/run/docker.sock"

Then:

    sudo service docker restart
    echo "export DOCKER_HOST=tcp://localhost:4243" >> ~/.bashrc
    source ~/.bashrc

Useful Commands
===============

Temporarily loading a MySQL dump into the DB:

    cat ~/django.sql | docker exec -i greendata_db_1 mysql django -uroot -p

Start bash on a container:

    docker exec -it greendata_web_1 bash -l

Create a user from the commandline. Needs to be done from the /code
directory on the web container:

    python manage.py createsuperuser

Resources
=========

The Docker and server configuration is based on:

* [Docker Explained: How To Containerize and Use Nginx as a Proxy](https://www.digitalocean.com/community/tutorials/docker-explained-how-to-containerize-and-use-nginx-as-a-proxy)
* [Django, uWSGI, and Nginx in a container](https://github.com/dockerfiles/django-uwsgi-nginx)
* [Setting up Django and your web server with uWSGI and nginx](https://uwsgi.readthedocs.org/en/latest/tutorials/Django_and_nginx.html)

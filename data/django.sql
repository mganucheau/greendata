-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: django
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add company',7,'add_company'),(20,'Can change company',7,'change_company'),(21,'Can delete company',7,'delete_company');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$nW1uN8jpODht$8OUIzEKcSJYfNfFH4ytZBkFmUJP24wruiLoEZqeaHwQ=','2015-04-16 20:59:51',1,'mganucheau','','','mganucheau@gmail.com',1,1,'2014-12-18 03:56:18'),(2,'pbkdf2_sha256$12000$GfZ4LJlrQnzm$83ks0wFcN+Xn64vFVv1c2TJGkbIZ7lMWSruIHw5dtG4=','2014-12-19 04:50:42',1,'nriley','','','neal.riley@gmail.com',1,1,'2014-12-18 03:56:37');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2015-02-18 14:16:42','18','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(2,'2015-02-18 14:17:19','17','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(3,'2015-02-18 14:18:01','16','Company object',2,'Changed cei, amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(4,'2015-02-18 14:18:40','15','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(5,'2015-02-18 14:20:05','14','Company object',2,'Changed twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(6,'2015-02-18 14:20:40','13','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(7,'2015-02-18 14:21:14','12','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(8,'2015-02-18 14:22:20','11','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(9,'2015-02-18 14:22:50','10','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(10,'2015-02-18 14:23:20','9','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(11,'2015-02-18 14:26:20','8','Company object',2,'Changed company, cei, amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(12,'2015-02-18 14:27:04','7','Company object',2,'Changed re_champion, amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(13,'2015-02-18 14:27:32','6','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(14,'2015-02-18 14:28:19','5','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(15,'2015-02-18 14:28:45','4','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(16,'2015-02-18 14:29:16','3','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(17,'2015-02-18 14:29:48','2','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(18,'2015-02-18 14:30:14','1','Company object',2,'Changed amount_gas, amount_coal, amount_nuclear, twitter_handle, competitor_one, competitor_two, competitor_three and service_host.',7,1),(19,'2015-02-19 10:35:31','18','reddit',2,'Changed company_url.',7,1),(20,'2015-02-19 10:35:38','17','pinterest',2,'Changed company_url.',7,1),(21,'2015-02-19 10:35:43','16','netflix',2,'Changed company_url.',7,1),(22,'2015-02-19 10:35:50','15','amazon',2,'Changed company_url.',7,1),(23,'2015-02-19 10:35:55','14','pandora',2,'Changed company_url.',7,1),(24,'2015-02-19 10:36:01','13','twitter',2,'Changed company_url.',7,1),(25,'2015-02-19 10:36:07','12','equinix',2,'Changed company_url.',7,1),(26,'2015-02-19 10:36:13','11','snapchat',2,'Changed company_url.',7,1),(27,'2015-02-19 10:36:19','10','instagram',2,'Changed company_url.',7,1),(28,'2015-02-19 10:36:26','9','ebay',2,'Changed company_url.',7,1),(29,'2015-02-19 10:36:32','8','bing',2,'Changed company_url.',7,1),(30,'2015-02-19 10:36:37','7','etsy',2,'Changed company_url.',7,1),(31,'2015-02-19 10:36:43','6','yahoo',2,'Changed company_url.',7,1),(32,'2015-02-19 10:36:51','5','rackspace',2,'Changed company_url.',7,1),(33,'2015-02-19 10:37:01','4','salesforce',2,'Changed company_url.',7,1),(34,'2015-02-19 10:37:07','3','google',2,'Changed company_url.',7,1),(35,'2015-02-19 10:37:12','2','facebook',2,'Changed company_url.',7,1),(36,'2015-02-19 10:37:18','1','apple',2,'Changed company_url.',7,1),(37,'2015-02-19 10:47:04','13','twitter',2,'Changed service_host.',7,1),(38,'2015-02-19 10:47:41','4','salesforce',2,'Changed service_host.',7,1),(39,'2015-02-19 18:54:44','18','reddit',2,'Changed competitor_one, competitor_two and competitor_three.',7,1),(40,'2015-02-19 18:55:36','17','pinterest',2,'Changed competitor_one and competitor_two.',7,1),(41,'2015-02-19 18:55:47','16','netflix',2,'No fields changed.',7,1),(42,'2015-02-19 18:56:12','15','amazon',2,'No fields changed.',7,1),(43,'2015-02-19 18:56:24','14','pandora',2,'Changed competitor_one and competitor_two.',7,1),(44,'2015-02-19 18:56:39','13','twitter',2,'Changed competitor_one and competitor_two.',7,1),(45,'2015-02-19 18:56:47','12','equinix',2,'No fields changed.',7,1),(46,'2015-02-19 18:57:26','10','instagram',2,'Changed competitor_one, competitor_two and competitor_three.',7,1),(47,'2015-02-19 18:57:50','9','ebay',2,'Changed competitor_one and competitor_two.',7,1),(48,'2015-02-19 18:58:05','8','bing',2,'No fields changed.',7,1),(49,'2015-02-19 18:58:20','7','etsy',2,'Changed competitor_one and competitor_two.',7,1),(50,'2015-02-19 18:58:34','6','yahoo',2,'Changed competitor_one and competitor_two.',7,1),(51,'2015-02-19 18:58:48','5','rackspace',2,'No fields changed.',7,1),(52,'2015-02-19 18:58:52','4','salesforce',2,'No fields changed.',7,1),(53,'2015-02-19 18:59:10','2','facebook',2,'Changed competitor_one and competitor_three.',7,1),(54,'2015-02-19 18:59:22','1','apple',2,'Changed competitor_one and competitor_two.',7,1),(55,'2015-02-19 20:11:22','3','google',2,'Changed competitor_one and competitor_two.',7,1);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'company','greenapi','company');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2014-12-18 03:54:19'),(2,'auth','0001_initial','2014-12-18 03:54:19'),(3,'admin','0001_initial','2014-12-18 03:54:20'),(4,'sessions','0001_initial','2014-12-18 03:54:20'),(5,'greenapi','0001_initial','2015-01-23 01:21:53'),(6,'greenapi','0002_auto_20150218_1359','2015-02-18 14:00:03'),(7,'greenapi','0003_company_company_url','2015-02-19 10:34:48');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('2ssd5y2ciy2m46f004y1ocydqjb5cn0e','MDQzMjZkMDBkNzFiYmE0ZWI0YjZiMGMzMTUxNTcyOTNlZDZjMTk3NDp7fQ==','2015-02-20 00:54:16'),('96qvjcisirivi7vbmdc2dw4mimbodzvy','MDdiNDQxYTIyNzE1MDMyNGQxOGU5ZGE5OTlhZjdkNjFhNDY4N2JhOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhMGQzOTIwMjg1YjMxMDQwZWMzNjhkNWZhNjVmMGIzODRhZDMwNWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-28 20:55:37'),('9fgo1erfafg8m19br2xxzfnhr200yyx2','YjAwOTI4ZGE5MzFlZmJkYWRiMjJjYTBkOGRhOTQ1ZDU3NmQ2NWMxNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjhmYmVlNDQwYzFkMWU4M2NhNGEzODIxZDc1MTEyMzg3M2Y0MmY4ZmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9','2015-01-02 04:50:42'),('aech2uczvg64vxk1e37t4cmqodo823rc','MDdiNDQxYTIyNzE1MDMyNGQxOGU5ZGE5OTlhZjdkNjFhNDY4N2JhOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhMGQzOTIwMjg1YjMxMDQwZWMzNjhkNWZhNjVmMGIzODRhZDMwNWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-01 04:09:51'),('ezs0gbz3bbwx9ehyowsmgf4e1j8fe6yw','MDQzMjZkMDBkNzFiYmE0ZWI0YjZiMGMzMTUxNTcyOTNlZDZjMTk3NDp7fQ==','2015-02-06 00:54:14'),('fnv4f283zop4rydqkw0puzumlwr8jnjj','MDdiNDQxYTIyNzE1MDMyNGQxOGU5ZGE5OTlhZjdkNjFhNDY4N2JhOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhMGQzOTIwMjg1YjMxMDQwZWMzNjhkNWZhNjVmMGIzODRhZDMwNWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-03-04 13:49:08'),('gdh9ygaasaw5z1jwsgets9k18ew3jmec','MDdiNDQxYTIyNzE1MDMyNGQxOGU5ZGE5OTlhZjdkNjFhNDY4N2JhOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhMGQzOTIwMjg1YjMxMDQwZWMzNjhkNWZhNjVmMGIzODRhZDMwNWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-04-30 20:59:51'),('j2ebn57zffpim6buo9botr8804ax3f5z','YjAwOTI4ZGE5MzFlZmJkYWRiMjJjYTBkOGRhOTQ1ZDU3NmQ2NWMxNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjhmYmVlNDQwYzFkMWU4M2NhNGEzODIxZDc1MTEyMzg3M2Y0MmY4ZmEiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9','2015-01-01 04:04:10'),('j9k7seiqqp9rfurwfprfvagch119t7cx','MDdiNDQxYTIyNzE1MDMyNGQxOGU5ZGE5OTlhZjdkNjFhNDY4N2JhOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhMGQzOTIwMjg1YjMxMDQwZWMzNjhkNWZhNjVmMGIzODRhZDMwNWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-02 18:02:59'),('no54z81qqxqm6syqf2kqb8nmid1wvc8y','MDdiNDQxYTIyNzE1MDMyNGQxOGU5ZGE5OTlhZjdkNjFhNDY4N2JhOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhMGQzOTIwMjg1YjMxMDQwZWMzNjhkNWZhNjVmMGIzODRhZDMwNWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-03-05 20:10:21'),('rx4pf0f0i5r89z7v276vatawhl68yaje','MDdiNDQxYTIyNzE1MDMyNGQxOGU5ZGE5OTlhZjdkNjFhNDY4N2JhOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhMGQzOTIwMjg1YjMxMDQwZWMzNjhkNWZhNjVmMGIzODRhZDMwNWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-01-02 17:08:20'),('saqbcj9xkmji54uwjj24iagro4m4xh83','MDQzMjZkMDBkNzFiYmE0ZWI0YjZiMGMzMTUxNTcyOTNlZDZjMTk3NDp7fQ==','2015-02-20 00:54:17'),('t5vjmxvf48mq0rttcytfuk2b0ewnivpn','MDdiNDQxYTIyNzE1MDMyNGQxOGU5ZGE5OTlhZjdkNjFhNDY4N2JhOTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNhMGQzOTIwMjg1YjMxMDQwZWMzNjhkNWZhNjVmMGIzODRhZDMwNWUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-03-22 10:16:32');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greenapi_company`
--

DROP TABLE IF EXISTS `greenapi_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greenapi_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(200) NOT NULL,
  `cei` int(11) DEFAULT NULL,
  `energy_transparency` tinyint(1) NOT NULL,
  `re_commitment` tinyint(1) NOT NULL,
  `re_champion` tinyint(1) NOT NULL,
  `raw_score` int(11) DEFAULT NULL,
  `grade` varchar(200) NOT NULL,
  `last_updated` datetime NOT NULL,
  `competitor_one` varchar(200) NOT NULL,
  `competitor_two` varchar(200) NOT NULL,
  `competitor_three` varchar(200) NOT NULL,
  `amount_coal` int(11),
  `amount_gas` int(11),
  `amount_nuclear` int(11),
  `service_host` varchar(200) NOT NULL,
  `twitter_handle` varchar(200) NOT NULL,
  `company_url` varchar(200),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greenapi_company`
--

LOCK TABLES `greenapi_company` WRITE;
/*!40000 ALTER TABLE `greenapi_company` DISABLE KEYS */;
INSERT INTO `greenapi_company` VALUES (1,'apple',100,1,1,1,100,'A','2014-12-19 12:59:00','pandora','spotify','null',0,0,0,'apple','#apple','www.apple.com'),(2,'facebook',49,1,1,1,75,'A','2015-12-18 00:00:00','instagram','twitter','pinterest',25,7,16,'facebook','@facebook','www.facebook.com'),(3,'google',48,1,1,1,74,'A','2014-12-18 00:00:00','yahoo','bing','null',22,13,15,'google','@google','www.google.com'),(4,'salesforce',28,1,1,1,64,'B','2014-12-18 00:00:00','null','null','null',22,17,26,'salesforce','@salesforce','www.salesforce.com'),(5,'rackspace',27,1,1,1,64,'B','2014-12-18 00:00:00','google','amazon','microsoft',30,26,17,'rackspace','@rackspace','www.rackspace.com'),(6,'yahoo',59,1,0,1,60,'B','2014-12-18 00:00:00','google','bing','null',20,6,12,'yahoo','@yahoo','www.yahoo.com'),(7,'etsy',16,1,1,1,58,'B','2014-12-18 00:00:00','ebay','amazon','null',27,29,23,'equinix','@etsy','www.etsy.com'),(8,'bing',29,1,0,1,45,'C','2014-12-18 00:00:00','google','yahoo','null',32,21,18,'microsoft','@bing','www.bing.com'),(9,'ebay',6,1,0,1,33,'C','2014-12-18 00:00:00','etsy','amazon','craigslist',24,47,14,'ebay','@ebay','www.ebay.com'),(10,'instagram',49,0,0,0,25,'C','2014-12-18 00:00:00','facebook','twitter','pinterest',25,7,16,'facebook','@instagram','www.instagram.com'),(11,'snapchat',48,0,0,0,24,'C','2014-12-18 00:00:00','instagram','pinterest','smugmug',22,13,15,'google','2snapchat','www.snapchat.com'),(12,'equinix',16,1,0,0,23,'C','2014-12-18 00:00:00','null','null','null',27,29,23,'null','@equinix','www.equinix.com'),(13,'twitter',21,0,0,0,11,'D','2014-12-18 00:00:00','facebook','instagram','reddit',22,42,15,'Twitter','@twitter','www.twitter.com'),(14,'pandora',17,0,0,0,9,'F','2014-12-18 00:00:00','apple','spotify','soundcloud',NULL,NULL,NULL,'null','@pandora_radio','www.pandora.com'),(15,'amazon',15,0,0,0,9,'F','2014-12-18 00:00:00','etsy','ebay','netflix',28,25,27,'Amazon Web Services','@amazon','www.amazon.com'),(16,'netflix',15,0,0,0,8,'F','2014-12-18 00:00:00','apple','amazon','hbo go',28,25,27,'Amazon Web Services','@netflix','www.netflix.com'),(17,'pinterest',15,0,0,1,8,'F','2014-12-18 00:00:00','facebook','instagram','flickr',28,25,27,'Amazon Web Services','@pinterest','www.pinterest.com'),(18,'reddit',15,0,0,0,8,'F','2014-12-18 00:00:00','facebook','twitter','imgur',28,25,27,'Amazon Web Services','@reddit','www.reddit.com');
/*!40000 ALTER TABLE `greenapi_company` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-23 23:13:55

FROM ubuntu:14.04

RUN apt-get update
RUN apt-get install -y aptitude
RUN aptitude update
RUN aptitude install -y build-essential git
RUN aptitude install -y python python-dev python-setuptools
RUN aptitude install -y nginx supervisor
RUN aptitude install -y python-pip
RUN aptitude install -y libmysqlclient-dev
RUN pip install --upgrade pip

# install uwsgi now because it takes a little while
RUN pip install uwsgi

# install our code
ADD . /home/docker/code/

# setup all the configfiles
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN rm /etc/nginx/sites-enabled/default
RUN ln -s /home/docker/code/etc/nginx-app.conf /etc/nginx/sites-enabled/
RUN ln -s /home/docker/code/etc/supervisor-app.conf /etc/supervisor/conf.d/

# run pip install
RUN pip install -r /home/docker/code/requirements.txt

RUN (cd /home/docker/code && python manage.py collectstatic --noinput)

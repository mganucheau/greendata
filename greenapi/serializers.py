from greenapi.models import Company
from rest_framework import serializers


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Company
        fields = ('id', 'company', 'company_url', 'cei', 'energy_transparency', 're_commitment', 're_champion', 'amount_gas', 'amount_coal', 'amount_nuclear', 'raw_score', 'grade', 'twitter_handle', 'competitor_one', 'competitor_two', 'competitor_three', 'service_host', 'last_updated')
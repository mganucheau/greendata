from django.db import models

class Company(models.Model):
    company = models.CharField(max_length=200)
    company_url = models.CharField(blank=True, null=True, max_length=200)
    cei =  models.IntegerField(blank=True, null=True)
    energy_transparency = models.BooleanField(default=None)
    re_commitment = models.BooleanField(default=None)
    re_champion = models.BooleanField(default=None)
    amount_gas = models.IntegerField(blank=True, null=True)
    amount_coal = models.IntegerField(blank=True, null=True)
    amount_nuclear = models.IntegerField(blank=True, null=True)
    raw_score =  models.IntegerField(blank=True, null=True)
    grade = models.CharField(max_length=200)
    twitter_handle = models.CharField(max_length=200)
    competitor_one = models.CharField(blank=True, max_length=200)
    competitor_two = models.CharField(blank=True, max_length=200)
    competitor_three = models.CharField(blank=True, max_length=200)
    service_host = models.CharField(max_length=200)
    last_updated = models.DateTimeField('last updated date')

    def __unicode__(self):
        return self.company

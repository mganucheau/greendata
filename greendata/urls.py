from django.conf.urls import url, include
from django.contrib import admin
from rest_framework import routers
from greenapi import views


router = routers.DefaultRouter()
router.register(r'companies', views.CompanyViewSet)

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^admin/', include(admin.site.urls)),
    url('^search/(?P<company>.+)/$', views.CompanyList.as_view()),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]